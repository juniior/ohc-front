'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  // API_URL: '"http://138.68.6.128/api/"',
  // API_FILES: '"http://138.68.6.128/storage/"'
  API_URL: '"http://localhost:8000/api/"',
  API_FILES: '"http://localhost:8000/storage/"'
})
