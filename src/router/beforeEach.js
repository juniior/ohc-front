import store from '../vuex'

const isAuthRoute = route => route.path.indexOf('/auth') !== -1
const isRegistroRoute = route => route.path.indexOf('/registro') !== -1
const isLogged = () => store.getters._isLogged

export default (to, from, next) => {
  if (!isAuthRoute(to) && !isLogged() && !isRegistroRoute(to)) {
    next('/auth')
  } else {
    next()
  }
}
