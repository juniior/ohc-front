import { routes as app } from '../app'

const root = [
  { path: '/', redirect: '/calendario' }
]

export default [ ...root, ...app ]
