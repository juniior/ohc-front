import Vue from 'vue'
import App from './app/Main.vue'
import router from './router'
import store from './vuex'
import Loader from './plugins/loader'
import VueTheMask from 'vue-the-mask'
import BootstrapVue from 'bootstrap-vue'
import VueSweetalert2 from 'vue-sweetalert2'
// import Select2 from 'v-select2-component'

import 'sweetalert2/dist/sweetalert2.min.css'

Vue.use(Loader, store)
Vue.use(VueTheMask)
Vue.use(BootstrapVue)
Vue.use(VueSweetalert2)
// Vue.component('Select2', Select2)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
