import { defaults } from 'lodash'
import axios from 'axios'

export const createClient = (options = { baseURL: process.env.API_URL }) => axios.create(defaults({}, options))

// createClient.interceptors.request.use(function (config) {
//   // Do something before request is sent
//   return config
// }, function (error) {
//   // Do something with request error
//   return Promise.reject(error)
// })

export default createClient()
