import module from './vuex'

const registerStore = store => {
  store.registerModule('JR_LOADER', { ...module })
}

export default registerStore
