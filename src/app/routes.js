import { routes as dashboard } from './dashboard'
import { routes as orquestra } from './orquestra'
import { routes as instrumento } from './instrumento'
import { routes as auth } from './auth'
import { routes as usuario } from './usuario'
import { routes as musica } from './musica'
import { routes as evento } from './evento'

export default [ ...auth, ...dashboard, ...orquestra, ...instrumento, ...usuario, ...musica, ...evento ]
