import { postNew, listaOrquestras, deleteOrquestra, loadOrquestra, editOrquestra, listaSolicitacoes, aceitarSolicitacao, recusarSolicitacao, listMusicos, listMembros } from '../services'

// lista
export const attemptAllOrquestras = (context) => {
  return listaOrquestras(JSON.parse(localStorage.getItem('user')))
    .then(resp => {
      context.commit('orquestra/setOrquestras', resp.data)
      return resp
    }, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptOrquestras.loadOrquestras', e])
    })
}

// cria
export const attemptNewOrquestra = (context, payload) => {
  return postNew(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptNewOrquestra.postNew', e])
    })
}

// remove
export const attemptRemoveOrquestra = (context, payload) => {
  return deleteOrquestra(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptRemoveOrquestra.deleteOrquestra', e])
    })
}

// carrega
export const attemptLoadOrquestra = (context, payload) => {
  return loadOrquestra(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptRemoveOrquestra.deleteOrquestra', e])
    })
}

// edita
export const attemptEditOrquestra = (context, payload) => {
  return editOrquestra(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptEditOrquestra.edit', e])
    })
}

// lista
export const attemptAllSolicitacoesOrquestra = (context) => {
  return listaSolicitacoes(context.getters._cPerfil.orquestra_id, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp.data, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.listaSolicitacoes', e])
    })
}

export const attemptAceitarSolicitacao = (context, payload) => {
  return aceitarSolicitacao(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptAceitarSolicitacao.edit', e])
    })
}

export const attemptRecusarSolicitacao = (context, payload) => {
  return recusarSolicitacao(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptRecusarSolicitacao.edit', e])
    })
}

// lista membros da orquestra
export const attemptAllMembros = (context) => {
  let user = JSON.parse(localStorage.getItem('user'))
  let orquestra = context.getters._cPerfil.orquestra_id

  return listMembros(user, orquestra)
    .then(resp => {
      context.commit('orquestra/setMembros', resp)
    }, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.list', e])
    })
}

// lista membros da orquestra
export const attemptAllMusicos = (context) => {
  let user = JSON.parse(localStorage.getItem('user'))
  let orquestra = context.getters._cPerfil.orquestra_id

  return listMusicos(user, orquestra)
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.list', e])
    })
}
