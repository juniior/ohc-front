import * as types from './mutations-types'

export default {
  [types.setMembros] (state, membros) {
    state.membros = membros
  },
  [types.setOrquestras] (state, orquestras) {
    state.orquestras = orquestras
  }
}
