import Main from './components/main'
import Form from './components/forms/form'
import Solicitacoes from './components/solicitacoes'
import Membros from './components/membros'
import fMembro from './components/forms/membro'

export default [
  { path: '/orquestra', component: Main, name: 'orquestra.main' },
  { path: '/orquestra/form', component: Form, name: 'orquestra.novo' },
  { path: '/orquestra/form/:id', component: Form, name: 'orquestra.editar' },

  { path: '/orquestra/solicitacoes', component: Solicitacoes, name: 'orquestra.solicitacoes' },

  { path: '/orquestra/membros', component: Membros, name: 'orquestra.membros' },
  { path: '/orquestra/membro/form/:id', component: fMembro, name: 'orquestra.membro.edit' }

]
