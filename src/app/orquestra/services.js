import http from '@/http'

export const postNew = (orquestra, user) => {
  return http.post(`orquestra`, orquestra, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['orquestra.services.new.catch', e])
    })
}

export const listaOrquestras = (user) => {
  return http.get('orquestra', { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data.data)
    .catch(e => {
      console.log(['orquestra.services.load.catch', e])
    })
}

export const deleteOrquestra = (id, user) => {
  return http.delete(`orquestra/` + id, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['orquestra.services.delete.catch', e])
    })
}

export const loadOrquestra = (id, user) => {
  return http.get('orquestra/' + id, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data.data)
    .catch(e => {
      console.log(['orquestra.services.load.catch', e])
    })
}

export const editOrquestra = (orquestra, user) => {
  return http.put(`orquestra/` + orquestra.id, orquestra, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.edit.catch', e])
    })
}

export const listaSolicitacoes = (orquestra, user) => {
  return http.get('solicitacao/listaPorOrquestra/' + orquestra, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(resp => resp)
    .catch(e => {
      console.log(['listaSolicitacoes', e])
    })
}

export const recusarSolicitacao = (id, user) => {
  return http.post(`solicitacao/recusar`, { id }, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.recusarSolicitacao.catch', e])
    })
}

export const aceitarSolicitacao = (id, user) => {
  return http.post(`solicitacao/aceitar`, { id }, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.aceitarSolicitacao.catch', e])
    })
}

export const listMembros = (user, orquestra) => {
  return http.get('membro/' + orquestra + '/orquestra', { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data.data, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['services.list.catch', e])
    })
}

export const listMusicos = (user, orquestra) => {
  return http.post('musicos', {orquestra}, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data.data, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['services.list.catch', e])
    })
}
