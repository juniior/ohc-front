import http from '@/http'

export const postNew = (instrumento, user) => {
  return http.post(`instrumento`, instrumento, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.new.catch', e])
    })
}

export const listInstrumentos = (user) => {
  return http.get('instrumento', { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data.data)
    .catch(e => {
      console.log(['services.list.catch', e])
    })
}

export const listInstrumentosTrashed = (user) => {
  return http.get('instrumento/trashed', { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data.data)
    .catch(e => {
      console.log(['services.listTrashed.catch', e])
    })
}

export const deleteInstrumento = (id, user) => {
  return http.delete(`instrumento/` + id, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.delete.catch', e])
    })
}

export const loadInstrumento = (id, user) => {
  return http.get('instrumento/' + id, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data.data)
    .catch(e => {
      console.log(['services.load.catch', e])
    })
}

export const editInstrumento = (instrumento, user) => {
  return http.put(`instrumento/` + instrumento.id, instrumento, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.edit.catch', e])
    })
}

export const restoreInstrumento = (id, user) => {
  return http.put(`instrumento/` + id + `/restore`, id, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.edit.catch', e])
    })
}
