import Main from './components/main'
import Removidos from './components/trashed'
import Form from './components/form'

export default [
  { path: '/instrumento', component: Main, name: 'instrumento.main' },
  { path: '/instrumento/form', component: Form, name: 'instrumento.novo' },
  { path: '/instrumento/form/:id', component: Form, name: 'instrumento.editar' },
  { path: '/instrumento/trashed', component: Removidos, name: 'instrumento.removidos' }
]
