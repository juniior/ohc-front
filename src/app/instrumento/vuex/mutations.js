import * as types from './mutations-types'

export default {
  [types.setInstrumentos] (state, instrumentos) {
    state.instrumentos = instrumentos
  }

}
