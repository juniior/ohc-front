import * as types from './mutations-types'
import { postNew, listInstrumentos, deleteInstrumento, loadInstrumento, editInstrumento, listInstrumentosTrashed, restoreInstrumento } from '../services'

// lista
export const attemptAllInstrumentos = (context) => {
  return listInstrumentos(JSON.parse(localStorage.getItem('user')))
    .then(data => {
      context.commit(types.setInstrumentos, data.data)
    }, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.listInstrumentos', e])
    })
}

// lista removidos
export const attemptAllInstrumentosTrashed = (context) => {
  return listInstrumentosTrashed(JSON.parse(localStorage.getItem('user')))
    .then(data => data, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.listInstrumentosTrashed', e])
    })
}

// cria
export const attemptNewInstrumento = (context, payload) => {
  return postNew(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => {
      context.commit('instrumento/setInstrumentos', resp.data)
      return resp
    }, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.postNew', e])
    })
}

// remove
export const attemptRemoveInstrumento = (context, payload) => {
  return deleteInstrumento(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => {
      context.commit('instrumento/setInstrumentos', resp.data)
      return resp
    }, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.deleteInstrumento', e])
    })
}

// carrega
export const attemptLoadInstrumento = (context, payload) => {
  return loadInstrumento(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.loadInstrumento', e])
    })
}

// edita
export const attemptEditInstrumento = (context, payload) => {
  return editInstrumento(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => {
      context.commit('instrumento/setInstrumentos', resp.data)
      return resp
    }, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.edit', e])
    })
}

// restaura
export const attemptRestoreInstrumento = (context, payload) => {
  return restoreInstrumento(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => {
      context.commit('instrumento/setInstrumentos', resp.data)
    }, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.restore', e])
    })
}
