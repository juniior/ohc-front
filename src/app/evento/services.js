import http from '@/http'

export const listByOrquestra = (dados, user) => {
  return http.get(`orquestra/` + dados.orquestra + `/eventos`, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.list', e])
    })
}

export const listAllEventosSistema = (user) => {
  return http.post(`evento/lista`, {user}, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.list', e])
    })
}

export const remove = (id, user) => {
  return http.delete(`evento/` + id, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.delete', e])
    })
}

export const add = (evento, orquestra, user) => {
  return http.post(`evento`, { evento, orquestra }, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.new', e])
    })
}

export const edit = (evento, user) => {
  return http.put(`evento/` + evento.id, evento, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.edit', e])
    })
}

export const load = (id, user) => {
  return http.get('evento/' + id, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data.data)
    .catch(e => {
      console.log(['services.load', e])
    })
}

export const loadMusicas = (id, user) => {
  return http.get('evento/' + id + '/musicas', { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data.data)
    .catch(e => {
      console.log(['services.load', e])
    })
}

export const addMusica = (dados, user) => {
  return http.post(`evento/musica`, dados, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.new', e])
    })
}

export const removeMusica = (id, user) => {
  return http.get(`evento/` + id + `/remove`, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.new', e])
    })
}

export const pdf = (dados, user) => {
  return http.get('evento/' + dados.evento + '/arranjo/' + dados.instrumento, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data.data)
    .catch(e => {
      console.log(['services.load', e])
    })
}

export const pdfAll = (id, user) => {
  return http.get('evento/' + id + '/arranjo', { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data.data)
    .catch(e => {
      console.log(['services.load', e])
    })
}
