import { listByOrquestra, listAllEventosSistema, remove, add, edit, load, loadMusicas, addMusica, removeMusica, pdf, pdfAll } from '../services'

// lista
export const attemptAllEventos = (context) => {
  let perfil = context.getters._cPerfil
  let user = JSON.parse(localStorage.getItem('user'))

  if (perfil.role.name === 'Administrador' && !perfil.orquestra) {
    return listAllEventosSistema(user)
      .then(resp => resp, (err) => {
        console.log(err)
      })
      .catch(e => {
        console.log(['actions', e])
      })
  } else {
    let orquestra = perfil.orquestra_id
    if (orquestra) {
      return listByOrquestra({ orquestra }, user)
        .then(resp => resp, (err) => {
          console.log(err)
        })
        .catch(e => {
          console.log(['actions', e])
        })
    } else {
      return new Promise(function () {

      }).catch(function (e) {
        console.log(e) // doesn't happen
      })
    }
  }
}

// remove
export const attemptRemoveEvento = (context, payload) => {
  return remove(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptRemoveEvento', e])
    })
}

// cria
export const attemptNewEvento = (context, payload) => {
  let orquestra = context.getters._cPerfil.orquestra_id
  return add(payload, orquestra, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptNewEvento', e])
    })
}
// carrega
export const attemptLoadEvento = (context, payload) => {
  return load(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptLoadEvento', e])
    })
}

// edita
export const attemptEditEvento = (context, payload) => {
  return edit(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptEditEvento', e])
    })
}

export const attemptAllMusicasEvento = (context, payload) => {
  return loadMusicas(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptAllMusicasEvento', e])
    })
}
export const attemptAddMusica = (context, payload) => {
  return addMusica(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptNewEvento', e])
    })
}

export const attemptRemoveMusicaEvento = (context, payload) => {
  return removeMusica(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptNewEvento', e])
    })
}

export const attemptPdfEvento = (context, payload) => {
  return pdfAll(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptEditEvento', e])
    })
}

export const attemptPdfEventoInstrumento = (context, payload) => {
  return pdf(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptEditEvento', e])
    })
}
