import Main from './components/main'
import Form from './components/forms/form'

import Calendario from './components/calendario'

export default [
  { path: '/eventos', component: Main, name: 'evento.main' },
  { path: '/evento/form', component: Form, name: 'evento.novo' },
  { path: '/evento/form/:id', component: Form, name: 'evento.editar' },

  { path: '/calendario', component: Calendario, name: 'evento.calendario' }

]
