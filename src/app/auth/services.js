import http from '@/http'

export const postLogin = (email, password) => {
  return http.post(`login`, {email: email, password: password})
    .then(response => response.data)
    .catch(e => {
      console.log(['services.postLogin', e])
    })
}

export const doLogoff = (user) => {
  return http.post(`usuario/logout`, user, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.doLogoff', e])
    })
}

export const create = (usuario) => {
  return http.post(`cadastro`, usuario)
    .then(response => response.data)
    .catch(e => {
      console.log(['services.create.catch', e])
    })
}

export const listRoles = (user) => {
  return http.get('role/list', { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data.data)
    .catch(e => {
      console.log(['services.listRoles.catch', e])
    })
}
