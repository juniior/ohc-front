import { isEmpty, isNull } from 'lodash'

export const _isLogged = ({ token }) => !isEmpty(token)

export const _cUser = ({ user }) => user
export const _cPerfil = ({ perfil }) => perfil
export const _allPerfis = ({ perfis }) => perfis

export const _cRoles = ({ roles }) => roles

export const _isAdmin = ({ perfil }) => !isNull(perfil) && isNull(perfil.orquestra_id) && isNull(perfil.instrumento_id) && (perfil.role_id === 1)
export const _isAdminOrquestra = ({ perfil }) => !isNull(perfil) && !isNull(perfil.orquestra_id) && isNull(perfil.instrumento_id) && (perfil.role_id === 1)
export const _isMusico = ({ perfil }) => !isNull(perfil) && !isNull(perfil.orquestra_id) && !isNull(perfil.instrumento_id) && (perfil.role_id === 3)
export const _isMaestro = ({ perfil }) => !isNull(perfil) && !isNull(perfil.orquestra_id) && isNull(perfil.instrumento_id) && (perfil.role_id === 2)
