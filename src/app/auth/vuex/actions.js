import * as types from './mutations-types'
import { postLogin, doLogoff, create, listRoles } from '../services'
import { isNull } from 'lodash'

export const attemptLogin = (context, payload) => {
  return postLogin(payload.email, payload.password)
    .then(resp => {
      if (resp.success) {
        context.commit(types.setToken, resp.data.user.token)
        context.commit(types.setUser, resp.data.user)
        context.commit(types.setAllPerfis, resp.data.perfis)
        localStorage.setItem('user', JSON.stringify(resp.data.user))

        resp.data.perfis.every(function (item) {
          if (item.is_default) {
            context.commit(types.setPerfil, item)
            return false
          }
          return true
        })

        if (isNull(context.state.perfil)) {
          context.commit(types.setPerfil, resp.data.perfis[0])
        }
      }
      return resp
    }, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptLogin', e])
    })
}

export const attemptLogoff = (context) => {
  return doLogoff(JSON.parse(localStorage.getItem('user')))
    .then(resp => {
      context.commit(types.setToken, null)
      context.commit(types.setUser, null)
      context.commit(types.setPerfil, null)
      context.commit(types.setAllPerfis, {})
      context.commit('musica/setMusicas', {})
      localStorage.clear()
    }, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.doLogoff', e])
    })
}

// carrega
export const attemptPerfil = (context, payload) => {
  context.commit('musica/setMusicas', {})

  context.getters._allPerfis.forEach(function (item) {
    if (payload === item.id) { context.commit(types.setPerfil, item) }
  })
}

export const attemptRegistro = (context, payload) => {
  return create(payload)
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.create', e])
    })
}

// lista
export const attemptAllRoles = (context) => {
  return listRoles(JSON.parse(localStorage.getItem('user')))
    .then(data => {
      context.commit(types.setRoles, data)
    }, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptAllRoles', e])
    })
}
