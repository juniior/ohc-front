import * as types from './mutations-types'

export default {
  [types.setUser] (state, user) {
    state.user = user
  },
  [types.setToken] (state, token) {
    state.token = token
  },
  [types.setPerfil] (state, perfil) {
    state.perfil = perfil
  },
  [types.setAllPerfis] (state, perfis) {
    state.perfis = perfis
  },

  [types.setRoles] (state, roles) {
    state.roles = roles
  },

  logoff (state) {
    state.token = ''
  }
}
