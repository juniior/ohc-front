export const setToken = 'auth/setToken'
export const setUser = 'auth/setUser'
export const setPerfil = 'auth/setPerfil'
export const setAllPerfis = 'auth/setAllPerfis'

export const setRoles = 'auth/setRoles'
