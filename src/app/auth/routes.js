import Main from './components/Main'
import Registro from './components/forms/Registro'

export default [
  { path: '/auth', component: Main },
  { path: '/registro', component: Registro }
]
