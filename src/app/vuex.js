import { vuex as auth } from './auth'
import { vuex as orquestra } from './orquestra'
import { vuex as instrumento } from './instrumento'
import { vuex as usuario } from './usuario'
import { vuex as musica } from './musica'
import { vuex as evento } from './evento'

export default { auth, orquestra, instrumento, usuario, musica, evento }
