import http from '@/http'

export const list = (orquestra, user) => {
  return http.get(`musica/listaPorOrquestra/` + orquestra, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.list', e])
    })
}

export const remove = (id, user) => {
  return http.delete(`musica/` + id, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.delete', e])
    })
}

export const add = (musica, orquestra, user) => {
  return http.post(`musica`, { musica, orquestra }, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.new', e])
    })
}

export const edit = (musica, user) => {
  return http.put(`musica/` + musica.id, musica, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.edit', e])
    })
}

export const load = (id, user) => {
  return http.get('musica/' + id, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data.data)
    .catch(e => {
      console.log(['services.load', e])
    })
}
export const addPartitura = (dados, user) => {
  return http.post(`partitura`, dados.formData, { headers: { Accept: 'application/json', 'Content-Type': 'multipart/form-data', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.new', e])
    })
}

export const listPartituras = (musica, user) => {
  return http.get(`musica/` + musica + `/partituras/`, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.list', e])
    })
}

export const download = (partitura, user) => {
  return http.get(`partitura/` + partitura, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.list', e])
    })
}

export const deletePartitura = (id, user) => {
  return http.delete(`partitura/` + id, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.deletePartitura', e])
    })
}
