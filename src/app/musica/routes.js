import Main from './components/main'
import Form from './components/forms/form'

export default [
  { path: '/musicas', component: Main, name: 'musica.main' },
  { path: '/musica/form', component: Form, name: 'musica.novo' },
  { path: '/musica/form/:id', component: Form, name: 'musica.editar' }

]
