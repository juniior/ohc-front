import * as types from './mutations-types'

export default {
  [types.setMusicas] (state, musicas) {
    state.musicas = musicas
  }
}
