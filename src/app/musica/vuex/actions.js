import { list, remove, add, edit, load, download, addPartitura, listPartituras, deletePartitura } from '../services'
import * as types from './mutations-types'
// lista
export const attemptAllMusicas = (context) => {
  let orquestra = context.getters._cPerfil.orquestra_id
  return list(orquestra, JSON.parse(localStorage.getItem('user')))
    .then(resp => {
      context.commit(types.setMusicas, resp.data)
    }, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptAllMusicas', e])
    })
}

// remove
export const attemptRemoveMusica = (context, payload) => {
  return remove(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => {
      context.commit(types.setMusicas, resp.data.musicas)
    }, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptRemoveMusica', e])
    })
}

// cria
export const attemptNewMusica = (context, payload) => {
  let orquestra = context.getters._cPerfil.orquestra_id
  return add(payload, orquestra, JSON.parse(localStorage.getItem('user')))
    .then(resp => {
      context.commit(types.setMusicas, resp.data.musicas)
      return resp
    }, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptNewMusica', e])
    })
}
// carrega
export const attemptLoadMusica = (context, payload) => {
  return load(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptLoadMusica', e])
    })
}

// edita
export const attemptEditMusica = (context, payload) => {
  return edit(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => {
      context.commit(types.setMusicas, resp.data.musicas)
    }, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptEditMusica', e])
    })
}

// cria
export const attemptNewPartitura = (context, payload) => {
  return addPartitura(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptNewPartitura', e])
    })
}

export const attemptAllPartituras = (context, payload) => {
  return listPartituras(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptAllMusicas', e])
    })
}

export const attemptDownloadPartitura = (context, payload) => {
  return download(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptDownloadPartitura', e])
    })
}

export const attemptRemovePartitura = (context, payload) => {
  return deletePartitura(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptRemovePartitura', e])
    })
}
