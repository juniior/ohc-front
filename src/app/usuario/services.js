import http from '@/http'

export const create = (usuario, user) => {
  return http.post(`usuario`, usuario, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.new.catch', e])
    })
}

export const list = (user) => {
  return http.get('usuario', { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data.data, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['services.list.catch', e])
    })
}

export const listTrashed = (user) => {
  return http.get('usuario/trashed', { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data.data, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['services.listTrashed.catch', e])
    })
}

export const remove = (id, user) => {
  return http.delete(`usuario/` + id, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.delete.catch', e])
    })
}

export const load = (id, user) => {
  return http.get('usuario/' + id, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data.data)
    .catch(e => {
      console.log(['services.load.catch', e])
    })
}

export const edit = (usuario, user) => {
  return http.put(`usuario/` + usuario.id, usuario, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.edit.catch', e])
    })
}

export const restore = (id, user) => {
  return http.put(`usuario/` + id + `/restore`, id, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.edit.catch', e])
    })
}

export const postNewSolicitacao = (codigo, user) => {
  return http.post(`solicitacao`, { codigo, user: user.id }, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['orquestra.services.new.catch', e])
    })
}

export const listaSolicitacoes = (user) => {
  return http.get('solicitacao/listaPorUsuario/' + user.id, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data.data)
    .catch(e => {
      console.log(['orquestra.services.listaSolicitacoes.catch', e])
    })
}

export const deleteSolicitacao = (id, user) => {
  return http.delete(`solicitacao/` + id, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['orquestra.services.delete.solicitacao', e])
    })
}

export const loadAllPerfis = (id, orquestra, user) => {
  return http.post('membro/perfis', { id, orquestra }, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data.data)
    .catch(e => {
      console.log(['orquestra.services.listaSolicitacoes.catch', e])
    })
}

export const createMebro = (membro, user, orquestra) => {
  return http.post(`membro`, { membro, orquestra }, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data)
    .catch(e => {
      console.log(['services.new.catch', e])
    })
}

export const removeMembro = (id, user) => {
  return http.delete(`membro/` + id, { headers: { Accept: 'application/json', Authorization: 'Bearer ' + user.token } })
    .then(response => response.data.data)
    .catch(e => {
      console.log(['services.delete.catch', e])
    })
}
