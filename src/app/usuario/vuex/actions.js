// import * as types from './mutations-types'
import { loadAllPerfis, create, createMebro, list, remove, removeMembro, load, edit, listTrashed, restore, postNewSolicitacao, listaSolicitacoes, deleteSolicitacao } from '../services'

// lista
export const attemptAllUsuarios = (context) => {
  return list(JSON.parse(localStorage.getItem('user')))
    .then(resp => {
      context.commit('usuario/setUsuarios', resp.data)
      return resp
    }, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.list', e])
    })
}

// lista removidos
export const attemptAllUsuariosTrashed = (context) => {
  return listTrashed(JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.listTrashed', e])
    })
}

// carrega
export const attemptLoadUsuario = (context, payload) => {
  return load(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.load', e])
    })
}

// cria
export const attemptNewUsuario = (context, payload) => {
  return create(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.create', e])
    })
}

// edita
export const attemptEditUsuario = (context, payload) => {
  let user = JSON.parse(localStorage.getItem('user'))
  return edit(payload, user)
    .then(resp => {
      if (resp.data.id === user.id) { context.commit('auth/setUser', resp.data.usuario) }
      return resp
    }, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.edit', e])
    })
}

// restaura
export const attemptRestoreUsuario = (context, payload) => {
  return restore(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.restore', e])
    })
}

// remove
export const attemptRemoveUsuario = (context, payload) => {
  return remove(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => {
      // context.commit(types.setListUsuario, resp.data)
      return resp
    }, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.remove', e])
    })
}

export const attemptNewSolicitacao = (context, payload) => {
  return postNewSolicitacao(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log('err', err)
    })
    .catch(e => {
      console.log(['actions.attemptNewSolicitacao', e])
    })
}

// lista
export const attemptAllSolicitacoes = (context) => {
  return listaSolicitacoes(JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.attemptOrquestras.loadOrquestras', e])
    })
}

export const attemptCancelaSolicitacao = (context, payload) => {
  return deleteSolicitacao(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
}

export const attemptAllPerfis = (context, payload) => {
  return loadAllPerfis(payload, context.getters._cPerfil.orquestra_id, JSON.parse(localStorage.getItem('user')))
    .then(resp => resp, (err) => {
      console.log(err)
    })
}

// cria novo membro
export const attemptNewMembro = (context, payload) => {
  let orquestra = (context.getters._isAdmin) ? payload.orquestra : context.getters._cPerfil.orquestra_id
  let user = JSON.parse(localStorage.getItem('user'))
  return createMebro(payload, user, orquestra)
    .then(resp => {
      if (resp.data.membro.user_id === user.id) {
        context.commit('auth/setAllPerfis', resp.data.perfis)
      }
      return resp
    }, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.createMebro', e])
    })
}

// remove
export const attemptRemoveMembro = (context, payload) => {
  let user = JSON.parse(localStorage.getItem('user'))
  return removeMembro(payload, JSON.parse(localStorage.getItem('user')))
    .then(resp => {
      if (resp.usuario.id === user.id) {
        context.commit('auth/setAllPerfis', resp.perfis)
      }
    }, (err) => {
      console.log(err)
    })
    .catch(e => {
      console.log(['actions.remove', e])
    })
}
