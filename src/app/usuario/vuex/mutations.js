import * as types from './mutations-types'

export default {
  [types.setUsuarios] (state, usuarios) {
    state.usuarios = usuarios
  }
}
