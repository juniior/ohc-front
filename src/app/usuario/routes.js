import Main from './components/main'
import Removidos from './components/trashed'
import Form from './components/forms/form'
import Perfil from './components/profile'
import Solicitacoes from './components/solicitacoes'

export default [
  { path: '/usuario/perfil', component: Perfil, name: 'usuario.perfil' },
  { path: '/usuario', component: Main, name: 'usuario.main' },
  { path: '/usuario/form', component: Form, name: 'usuario.novo' },
  { path: '/usuario/form/:id', component: Form, name: 'usuario.editar' },
  { path: '/usuario/trashed', component: Removidos, name: 'usuario.removidos' },
  { path: '/musico', component: Main, name: 'usuario.musicos' },
  { path: '/musico/trashed', component: Removidos, name: 'usuario.musicos.removidos' },
  { path: '/musico/form', component: Form, name: 'usuario.musicos.novo' },
  { path: '/musico/form/:id', component: Form, name: 'usuario.musicos.editar' },

  { path: '/usuario/solicitacoes', component: Solicitacoes, name: 'usuario.solicitacoes' }

]
